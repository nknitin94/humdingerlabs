##This module consists of functions that aggregate data from lower level summaries to higher level summaries
##Required Inputs would be the date/date_range and tw_handle

import pymongo
from dbconfig import *

class aggregateLowerToHigher(object):

    def __init__(self):
        pass

    def get_db_connection(self,database):
        self.client = pymongo.MongoClient(config_db["host"],config_db["port"])
        self.db = self.client[database]
        self.database = database
        return

    def get_tw_handle(self,tw_handle):
        self.tw_handle = tw_handle
        return

    def retrieve_relevant_document(self,date):

        #getting date/date_range for which aggregations are to take place
        self.date = date

        #setting up temp db for aggregation purposes
        self.temp_db = self.client["Process_Data"]
        self.temp_coll = self.temp_db[self.tw_handle]

        #retriving required document
        record = self.db[tw_handle].find_one({"date":self.date})
##        print record

        #inserting record in temp
        self.temp_coll.insert(record)

        #setting summary_query
        if self.database == "daily_summary":
            self.query = "Hours"
        elif self.database == "weekly_summary":
            self.query = "Days"
        elif self.database == "monthly_summary":
            self.query = "Weeks"
        
        return

    def cleanup_temp_db(self):
        #delete temp_db and temp_coll
        self.temp_coll.remove({})
        return

    def agg_sentiments(self):

        pipeline = [
            {"$unwind":"$"+self.query},
            {
                "$group":
                {
                    "_id":"$"+self.query+".sentiments._id",
                    "positive":{"$sum":"$"+self.query+".sentiments.positive"},
                    "negative":{"$sum":"$"+self.query+".sentiments.negative"},
                    "neutral":{"$sum":"$"+self.query+".sentiments.neutral"}
                }
            }
        ]

        result = self.temp_coll.aggregate(pipeline)

        sentiAgg = list(result)

        #inserting aggreagated sentiments to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"sentiments":sentiAgg[0]}})

    def  agg_topics(self):
        #add code to create a dictionary of topics with keys as topics and values as frequecny
        topicList = self.temp_coll.distinct(self.query+'.topics')

        tempDict = dict()
        topicDict = dict()
        
        for i in range(len(topicList)):
            tempDict = topicList[i]
            for key in tempDict.keys():
                if key not in topicDict:
                    topicDict[key] = 0
                topicDict[key] += tempDict[key]
                
        #inserting aggreagated topics to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"topics":topicDict}})

    def agg_mentions(self):
        mentionList = self.temp_coll.distinct(self.query+'.basic_tw_stats.mentions')

        tempDict = dict()
        mentionDict = dict()
        
        for i in range(len(mentionList)):
            tempDict = mentionList[i]
            for key in tempDict.keys():
                if key not in topicDict:
                    mentionDict[key] = 0
                mentionDict[key] += tempDict[key]
                
        #inserting aggreagated mentions to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"basic_tw_stats.mentions":mentionDict}})

    def agg_hashtags(self):
        hashtagList = self.temp_coll.distinct(self.query+'.basic_tw_stats.hashtags')

        tempDict = dict()
        hashtagDict = dict()
        
        for i in range(len(hashtagList)):
            tempDict = hashtagList[i]
            for key in tempDict.keys():
                if key not in hashtagDict:
                    hashtagDict[key] = 0
                hashtagDict[key] += tempDict[key]
                
        #inserting aggreagated hashtags to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"basic_tw_stats.hashtags":hashtagDict}})

    def agg_urls(self):
        urlList = self.temp_coll.distinct(self.query+'.urls')

        tempDict = dict()
        urlDict = dict()
        
        for i in range(len(urlList)):
            tempDict = urlList[i]
            for key in tempDict.keys():
                if key not in urlDict:
                    urlDict[key] = 0
                urlDict[key] += tempDict[key]
                
        #inserting aggreagated urls to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"basic_tw_stats.urls":urlDict}})

    def agg_tw_metrics(self):

        ##do not call this function for aggregating Hourly data to daily summary level
        records = self.temp_coll.distinct(self.query'.tw_metrics')

        tempRecord = dict()
        metricsDict = dict()
        
        for itr in range(len(records)):
            tempRecord = records[itr]
            for key in tempRecord.keys():
                if key not in metricsDict:
                    metricsDict[key] = 0
                metricsDict[key] += tempRecord[key]

        #Normalizing values of added metric values in metricsDict
        for key in metricsDict.keys():
            metricsDict[key] = metricsDict[key]/len(records)

        #inserting aggreagated tw_metrics to higher summary level
        self.db[self.database][self.tw_handle].update({"date":self.date},{"$set":{"tw_metrics":metricsDict}})
        

    
