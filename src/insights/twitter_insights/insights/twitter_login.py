import twitter
import oauth2

def oauth_login(consumer_key,consumer_secret,oauth_token,oauth_token_secret):
    
    CONSUMER_KEY= consumer_key
    CONSUMER_SECRET= consumer_secret
    OAUTH_TOKEN= oauth_token
    OAUTH_TOKEN_SECRET= oauth_token_seret

    auth=twitter.oauth.OAuth(OAUTH_TOKEN,OAUTH_TOKEN_SECRET,CONSUMER_KEY, CONSUMER_SECRET)
    twitter_api=twitter.Twitter(auth=auth)
    return twitter_api
