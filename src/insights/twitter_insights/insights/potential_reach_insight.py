''' This code measures the potential reach of a brand.Potential reach is measured as the sum of all users mentioning the brand + the sum of their followers.'''
''' The input to this code are the twitter credentials(consumer_key,consumer_secret,oauth_token,oauth_token_secret)and the brand name or the twitter handle'''
''' for which the user wants to calculate the reach and tweets(optional).If tweets(raw fomat) are given as input the code will not fetch tweets from twitter'''
''' The input tweets should be the tweets where the brand name or the handle is mentioned'''
''' The output is a number indicating the potential reach'''


import json
import io
import pymongo
from get_followers_friends import usage
from robust_twitter_requests import make_twitter_request
from twitter_login import oauth_login



def save_to_mongo(data,mongo_db,mongo_db_coll,**mongo_conn_kw):

    client=pymongo.MongoClient(**mongo_conn_kw)
    db=client[mongo_db]
    coll=db[mongo_db_coll]
    coll.insert(data)
    print "data inserted in db....."
    print


def load_from_mongo(mongo_db,mongo_db_coll,return_cursor=False,criteria=None,projection=None,**mongo_conn_kw):

    screen_names=list()
    client= pymongo.MongoClient(**mongo_conn_kw)
    db=client[mongo_db]
    coll=db[mongo_db_coll]

    if criteria is None:
        criteria={}

    tweets_iterator=coll.find(criteria)
    i=0
    for tweet in tweets_iterator:

        
        
        user_screen_name=tweet['user']['screen_name']
        screen_names.append(user_screen_name)        
        
    return screen_names

def twitter_search(twitter_api,q,max_results=200,**kw):

    search_results=make_twitter_request(twitter_api.search.tweets,q=q,count=100)
    statuses=search_results['statuses']

    max_results=min(1000,max_results)

    for _ in range(8):

        try:
            next_results=search_results['search_metadata']['next_results']
        except KeyError,e:
            break

        kwargs=dict([kv.split('=')for kv in next_results[1:].split("&")])
        search_results=twitter_api.search.tweets(**kwargs)
        statuses+=search_results['statuses']

        if len(statuses)>max_results:
            break

    return statuses


def potential_reach(twitter_api,query,tweets=None):

    q=query
    no_of_followers=int(0)
    no_of_users=int(0)
    screen_name_followers=dict()
    screen_names=dict()
    twitter_api = twitter_api
    
    if tweets==None: #if no tweets are supplied as input,tweets will be fetched from twitter
        results= twitter_search(twitter_api,q,max_results=10) # fetch the tweets mentioning the brand or the specified twitter handle
        save_to_mongo(results,'potential_reach',q)
        screen_name_followers=load_from_mongo('potential_reach',q)
        no_of_users=len(screen_name_followers)#no. of users mentioning the brand name

    elif tweets!=None:
        for tweet in tweets:
            user_screen_name=tweet['user']['screen_name']
            screen_name_followers.apppend(user_screen_name)  
        no_of_users=len(screen_name_followers)#no. of users mentioning the brand name
    

    for screen_name in screen_name_followers:
        
        try:                                     
            followers_id=usage(screen_name,twitter_api=twitter_api) #get the followers of the users who has mentioned the brand name or the handle
            no_of_followers+=len(followers_id)

        except:
            continue


    potential_reach=no_of_users+no_of_followers

    text= "potential reach of "+q+" is "+str(potential_reach)
    return potential_reach


