''' This code measures the busiest and most idle day of the week from a given set of tweets'''
''' The input to this code are the twitter credentials consumer key,consumer secret,oauth token,oauth token secret and the concerned brand's screen_name'''
''' The output is in json '''
''' To calculate the busiest day of the week all the follwers of the brand are retrieved and then for each follower a set of 200 tweets are retrieved and then the'''
''' tweets are analysed for the busiest hour'''


import json
import os
import io
import pymongo

from get_followers_friends import *
from robust_twitter_requests import make_twitter_request
from harvest_tweets import *
import numpy as np
from twitter_login import oauth_login


def save_to_mongo(data,mongo_db,mongo_db_coll,**mongo_conn_kw):

    client=pymongo.MongoClient(**mongo_conn_kw)
    db=client[mongo_db]
    coll=db[mongo_db_coll]
    coll.insert(data)
    print "data inserted in db....."
    print

def load_from_mongo(mongo_db,mongo_db_coll,return_cursor=False,criteria=None,projection=None,**mongo_conn_kw):

    days=list()
    client= pymongo.MongoClient(**mongo_conn_kw)
    db=client[mongo_db]
    coll=db[mongo_db_coll]

    if criteria is None:
        criteria={}

    tweets_iterator=coll.find(criteria)
    i=0
    for tweet in tweets_iterator:       
        
        time=tweet['created_at']
        t1= time.split(" ")
        d = t1[0]
        
        days.append((str(d)))
        
    return days       



def days_insight(twitter_api,brand_screen_name):


    q= brand_screen_name
    dirname="F:\\Humdinger Labs\\codebase\\" #change the dirname to the name of the current directory path
    filename=q +'_days_insight.png'
    twitter_api=twitter_api

    
    followers_ids=usage(q,twitter_api=twitter_api) #get the followers of the brand
    time=list()

    for follower in followers_ids: #for each follower get tweets and store them in mongodb
        try:
            data=get_tweets(twitter_api,user_id=follower)
            save_to_mongo(data,'days_tweets',q)
        except:
            continue

    a=list()
    b=list()
    busiest_day=''
    idle_day=''
    time=load_from_mongo('temporal_tweets',q)
    time.sort()
    X= {x:time.count(x) for x in time}
    a, b = X.keys(), X.values()
    S=map(lambda x:str(x),a)
    data=list()
    l=len(S)
    num=range(1,l+1)
    i=0
    for t in S:
        data.append((t,num[i]))
        i+=1

    values=X.values()
    values.sort()
    temp_time=values[len(a)-1]

    for x in X.keys():
        if X[x]==temp_time:
            busiest_day=x
            break
        
    temp_time=values[0]
    for x in X.keys():
        if X[x]==temp_time:
            idle_day=x
            break
    
        
##    N = len( data )
##    x = np.arange(1, N+1)
##    y = [ num for (s, num) in data ]
##    labels = [ s for (s, num) in data ]
##    width = 1
##    fig=plt.figure(figsize=(20,10))
##    bar1 = plt.bar( x, b, width, color="y" )
##    plt.title("Twitter activity")
##    plt.ylabel( 'No. of tweets' )
##    plt.xticks(x + width/2.0, labels,rotation=45)
##    savepath=os.path.join(dirname,filename)
##    fig.savefig(savepath,dpi=200)
##    plt.close()

    text1="The busiest day of the week is " + busiest_day
    text2="The most idle day of the week is " + idle_day
    result={'insight':'busiest_day_of_week',
            'histogram':X,
            'text1':text1,
            'text2':text2}

    return json.dumps(result)
              
    
    
    


      

