from robust_twitter_requests import make_twitter_request
import io

def harvest_user_timeline(twitter_api, screen_name=None, user_id=None, max_results=1000):
     
    assert (screen_name != None) != (user_id != None), \
    "Must have screen_name or user_id, but not both"    
    
    kw = {  # Keyword args for the Twitter API call
        'count': 200,
        'trim_user': 'true',
        'include_rts' : 'true',
        'since_id' : 1
        }
    
    if screen_name:
        kw['screen_name'] = screen_name
    else:
        kw['user_id'] = user_id
        
    max_pages = 16
    results = []
    
    tweets = make_twitter_request(twitter_api.statuses.user_timeline, **kw)
    
    if tweets is None: # 401 (Not Authorized) - Need to bail out on loop entry
        tweets = []
        
    results += tweets
    
    print >> sys.stderr, 'Fetched %i tweets' % len(tweets)
    
    page_num = 1
    
    
    
    if max_results == kw['count']:
        page_num = max_pages # Prevent loop entry
    
    while page_num < max_pages and len(tweets) > 0 and len(results) < max_results:
    
        # Necessary for traversing the timeline in Twitter's v1.1 API:
        # get the next query's max-id parameter to pass in.
        # See https://dev.twitter.com/docs/working-with-timelines.
        kw['max_id'] = min([ tweet['id'] for tweet in tweets]) - 1 
    
        tweets = make_twitter_request(twitter_api.statuses.user_timeline, **kw)
        results += tweets

        print >> sys.stderr, 'Fetched %i tweets' % (len(tweets),)
    
        page_num += 1
        
    print >> sys.stderr, 'Done fetching tweets'

    return results[:max_results]
    
# Sample usage
def get_tweets(twitter_api,screen_name=None,user_id=None):
    twitter_api = twitter_api

    if screen_name:
        ScreenName=screen_name
        tweets = harvest_user_timeline(twitter_api, screen_name=ScreenName, \
                               max_results=200)
        return tweets

    else:
        UserId=user_id
        tweets = harvest_user_timeline(twitter_api, user_id=UserId, \
                               max_results=200)
        return tweets

##if __name__ == '__main__':
##
##    filename='test_tweets.csv'
##    data=get_tweets('SocialWebMining')
##    
##    with io.open(filename,'w',encoding='utf-8') as f:
##        f.write(unicode(json.dumps(data,ensure_ascii=False)))
##
##    print "done..."    
##    
# Save to MongoDB with save_to_mongo or a local file with save_json...
