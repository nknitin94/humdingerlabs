''' this code measures the engagement percent and impression of a brand.engament % = engagement/impression'''
''' engagement = @Replies + Retweets + Mentions + Favorites'''
''' impression = The total number of times a Tweet from brand's account or mentioning the brandd could appear in users� Twitter feeds during the report period.'''
''' It includes the brand's Tweets, Tweets that mention the brand handle, and retweets '''
''' The input to this code are the twitter credentials,tweets mentioning the brand name or handle,and the screen name of the brand'''

import twitter
import oauth2
import io

from robust_twitter_requests import *
from twitter_login import oauth_login

def harvest_user_timeline(twitter_api, screen_name=None, user_id=None, max_results=1000):
     
    assert (screen_name != None) != (user_id != None), \
    "Must have screen_name or user_id, but not both"    
    
    kw = {  # Keyword args for the Twitter API call
        'count': 200,
        'trim_user': 'true',
        'include_rts' : 'true',
        'since_id' : 1
        }
    
    if screen_name:
        kw['screen_name'] = screen_name
    else:
        kw['user_id'] = user_id
        
    max_pages = 16
    results = []
    
    tweets = make_twitter_request(twitter_api.statuses.user_timeline, **kw)
    
    if tweets is None: # 401 (Not Authorized) - Need to bail out on loop entry
        tweets = []
        
    results += tweets
    
    print >> sys.stderr, 'Fetched %i tweets' % len(tweets)
    
    page_num = 1
    
    
    if max_results == kw['count']:
        page_num = max_pages # Prevent loop entry
    
    while page_num < max_pages and len(tweets) > 0 and len(results) < max_results:
    
        # Necessary for traversing the timeline in Twitter's v1.1 API:
        # get the next query's max-id parameter to pass in.
        # See https://dev.twitter.com/docs/working-with-timelines.
        kw['max_id'] = min([ tweet['id'] for tweet in tweets]) - 1 
    
        tweets = make_twitter_request(twitter_api.statuses.user_timeline, **kw)
        results += tweets

        print >> sys.stderr, 'Fetched %i tweets' % (len(tweets),)
    
        page_num += 1
        
    print >> sys.stderr, 'Done fetching tweets'

    return len(results)




##def load_from_mongo(mongo_db,mongo_db_coll,return_cursor=False,criteria=None,projection=None,**mongo_conn_kw):
##
##    screen_names=list()
##    client= pymongo.MongoClient(**mongo_conn_kw)
##    db=client[mongo_db]
##    coll=db[mongo_db_coll]
##
##    if criteria is None:
##         tweets_iterator=coll.find()         
##         return tweets_iterator
##
##    elif criteria=="count":
##        return coll.find().count()

def find_retweeted_tweets(tweets, retweet_threshold=2):

    statuses=tweets
    count=int(0)

    for tweets in statuses:
        if tweets['retweet_count'] > retweet_threshold:
            count+=1
        
    return count


def engagement_rate(twitter_api,tweets,ScreenName):

    no_of_retweets= find_retweeted_tweets(tweets) #out of all the tweets how many of them retweeted
    mentions=int(0)
    
    for tweet in tweets:
        mentions+=1 #total no. of tweets mentioning the brand handle
        
    #mentions=load_from_mongo(tweets,mongo_db,handle,criteria="count") #total no. of tweets mentioning the brand handle
        
    twitter_api=oauth_login(consumer_key,consumer_secret,oauth_token,oauth_token_secret)
    favorites=len(twitter_api.favorites.list(screen_name=ScreenName, count=200)) #no. of favourite tweets

    engagement=no_of_retweets + mentions + favorites
    

    user_tweets=harvest_user_timeline(twitter_api, screen_name=ScreenName,max_results=200)
    impression=user_tweets + mentions + no_of_retweets
    

    engagement_percent=float(0.0)
    engagement_percent= float(float(engagement)/float(impression)) *100

    text1= "engament is: " + str(engagement_percent) +"%"
    text2= "impression is: " + str(impression)
    result = {'engagement':text1,
              'impression':text2}
    
    return json.dumps(result)

    
##if  __name__ == '__main__':
##
##    engagement_rate("potential_reach","@POTUS","President Obama")

    
