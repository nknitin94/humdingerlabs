import json
import pymongo

def get_details(username,mongo_db,mongo_db_coll,**mongo_conn_kw):
	
	user_id=username
	client=pymongo.MongoClient(**mongo_conn_kw)
	db=client[mongo_db]
        coll=db[mongo_db_coll]
	data=coll.find({"cust_id": user_id})
	result=json.dumps(data)
	return result
	
	
