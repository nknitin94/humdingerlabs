import pymongo
from dbconfig import *

class DBService(object):
    def __init__(self):
        pass
    
    def get_db_connection(self):
        self.client = pymongo.MongoClient(config_db["host"],config_db["port"])
        return
    
    def close_db_connection():
        self.client.close()
        return

    def authenticate_user(self,cust_id,username,passwd):

        #extract relevant database from cust_id in var <database>
        domain = cust_id.split('@')
        domain = domain[1].split('.')
        database = domain[0]

        #connecting to relevant database
        db = self.client[database]
        dbCollection = db.cust_details

        output = dict()
        output['status'] = dict()
        output['status']['status_code'] = False

        record = dbCollection.find_one({"cust_id":cust_id})

        if record == None:
            # some status code
        else:
            emailList = record["allowed_emails"]
            for emailInfo in emailList:
                if emailInfo["email"] == username and emailInfo["passwd"] == passwd:
                    output['status']['status_code'] == True
                    return output

        return output

    def check_if_user_exits(self,cust_id):

        #extract relevant database from cust_id in var <database>
        domain = cust_id.split('@')
        domain = domain[1].split('.')
        database = domain[0]

        #connecting to relevant database
        db = self.client[database]
        dbCollection = db.cust_details

        record = dbCollection.find_one({"cust_id":cust_id})

        if record == None:
            return False
        else:
            return True

    

        
        
