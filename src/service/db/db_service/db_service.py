import pymongo

class DBService(object):
    def __init__(self):
        pass
    
    def __init__():
        
        self.client = pymongo.MongoClient()
        self.db = self.client[dataBase]
        self.dbCollection = self.db[collection]
##        pass

    def authenticate_user(self,cust_id,username,passwd):

        ##connecting to the required database and collection
        db = self.client['digital_upstarts']
        dbCollection = db[cust_details]
        
        output = dict()
        output['status'] = dict()
        output['status']['status_code'] = False

        record = dbCollection.find_one({"cust_id":cust_id})

        if record == None:
            # some status code
        else:
            emailList = record.allowed_emails
            for emailInfo in emailList:
                if emailInfo["email"] == username and emailInfo["passwd"] == passwd:
                    output['status']['status_code'] == True
                    return output

        return output
        

    def check_if_user_exits(self,email_ID):
        ##connecting to the required database and collection
        db = self.client['digital_upstarts']
        dbCollection = db[cust_details]

        record = dbCollection.find_one({"cust_id":cust_id})

        if record == None:
            return False
        else:
            return True

    def register_user(self,inputs):
        ##add code to register user
        

    def sentiments(self, queryDict):
    ##Generates labels (positive, negative and neutral) and calculates values (count for each label) throughout all hours of the day

        
        customer = queryDict["sentiments"]
        query = "$"+queryDict["summary"]+"."+customer
        pipeline = [
            { "$unwind":query },
            {
                "$group":
                {
                    "_id": query+".Insights.sentiments._id",
                    "positive":{"$sum":query+".Insights.sentiments.pos"},
                    "negative":{"$sum":query+".Insights.sentiments.neg"},
                    "neutral":{"$sum":query+".Insights.sentiments.neu"}
                }
            }
        ]

        result = self.dbCollection.aggregate(pipeline)

        sentiAgg = list(result)
        del sentiAgg[0]["_id"]

        labels = sentiAgg[0].keys()
        values = sentiAgg[0].values()

        return (labels,values)
    

    def allFrequency(self, queryDict):
    ##Calculates the frequency of tweets from different cities in a day 

        TopCount = -2        #change this value for getting different number of top active cities to be displayed
        BottomCount = 2    #change this value for getting different number least active cities to be displayed
        
        customer = queryDict["allFrequency"]
        query = queryDict["summary"]+"."+customer+".Insights.geo.locations"
        cityList = self.dbCollection.distinct(query)
        cityFreq = dict()

        for i in range(len(cityList)):
            listValue = cityList[i]
            for key in listValue:
                if key != "_id":
                    if key not in cityFreq:
                        cityFreq[key] = 0
                    else:
                        cityFreq[key] += listValue[key]

        #sorting city frequency based on top five and bottom five cities
        cityTopFreq = dict()
        cityBottomFreq = dict()

        cityTopFreq["Others"] = 0
        cityBottomFreq["Others"] = 0
        
        (keys, values) = cityFreq.keys(),cityFreq.values()
        values.sort()
        Top = values[TopCount:]
        Bottom = values[:BottomCount]

        for itr in range(len(Top)):
            for key in cityFreq.keys():
                if cityFreq[key] == Top[itr]:
                    if key not in cityTopFreq:
                        cityTopFreq[key] = Top[itr]

        for itr in range(len(Bottom)):
            for key in cityFreq.keys():
                if cityFreq[key] == Bottom[itr]:
                    if key not in cityBottomFreq:
                        cityBottomFreq[key] = Bottom[itr]

        for key in cityFreq.keys():
            if key not in cityTopFreq:
                cityTopFreq["Others"] += cityFreq[key]

        for key in cityFreq.keys():
            if key not in cityBottomFreq:
                cityBottomFreq["Others"] += cityFreq[key]
                
        print cityFreq

        return (cityTopFreq,cityBottomFreq)
    

    def hrFrequency(self, queryDict):
    ##Calculates hourly frequency counts of tweets from different cities throughout all hours of the day
    
        customer = queryDict["hrFrequency"]
        query = queryDict["summary"]+"."+customer+".Insights.geo.locations"
        cityList = self.dbCollection.distinct(query)
                
        for i in range(len(cityList)):
            if "_id" in cityList[i]:
                del cityList[i]["_id"]
                
        cityHrDict = dict()
        xlabels = list()
                
        for i in range(len(cityList)):
            xlabels.append('hr_'+str(i+1))
            cityDict = cityList[i]

            for key in cityDict.keys():
                if key not in cityHrDict:
                    cityHrDict[key] = list()
                            
                if len(cityHrDict[key]) != i:
                    for times in range(i - len(cityHrDict[key])):
                        cityHrDict[key].append(0)
                    
                cityHrDict[key].append(cityDict[key])
                
        return (cityHrDict,xlabels)
    

    def tagcloud(self, queryDict):
    ##Generates a tag cloud using topics field from each hour
    
        customer = queryDict["tagcloud"]
        query = "$"+queryDict["summary"]+"."+customer+".Insights.topics"

        pipeline = [
            { "$unwind":"$"+queryDict["summary"]+"."+customer },
            { "$unwind":"$"+queryDict["summary"]+"."+customer+".Insights.topics" },
            { "$group":{"_id":query,"count":{"$sum":1}} }
        ]
                
        topicList = list(self.dbCollection.aggregate(pipeline))
                
        textList = list()
        textString = ""
        for i in range(len(topicList)):
            textDict = topicList[i]
            for j in range(textDict["count"]):
                if ' ' in textDict["_id"]:
                    textDict["_id"] = textDict["_id"].replace(' ','_')
                            
                textList.append(textDict["_id"])
                
        for word in textList:
            textString += word+", "

        return textString
    

    def summary_3600(self, queryDict):

        if "sentiments" in queryDict:
            return self.sentiments(queryDict)

        elif "allFrequency" in queryDict:
            return self.allFrequency(queryDict)

        elif "hrFrequency" in queryDict:
            return self.hrFrequency(queryDict)

        elif "tagcloud" in queryDict:
            return self.tagcloud(queryDict)


    def get_data_db(self,queryDict):

        summary_field = queryDict["summary"]

        if summary_field == "summary_3600":

            return self.summary_3600(queryDict)

        ## add other branches for different summary fields
        ## if summary_field == "summary_weekly"
        ##      ..............
        ## if summary_field == "summary_monthly"
        ##      ..............


##def main():
##    DBS = DBService("TwitterTest","HUL_UPDATE")
##    queryDict = dict()
##    queryDict["summary"] = "summary_3600"
##    queryDict["allFrequency"] = "HUL"
##
##    (Top,Bottom) = DBS.get_data_db(queryDict)
##
##    print Top
##    print Bottom
###    print labels
##
##if __name__ == '__main__':
##    main()
