##This module consists of functions that sends aggregated data to the reporting module
##for day by day plots of a week specified in "date" parameter

import pymongo
from dbconfig import *

class DailySummary(object):
    def __init__(self):
        pass
    
    def get_db_connection(self):
        self.client = pymongo.MongoClient(config_db["host"],config_db["port"])
        self.db = self.client["weekly_summary"]
        return
    
    def get_tw_handles(self,tw_handles):
        self.tw_handles = tw_handles
        return

    def retrieve_week_document(self,date,tw_handle):

        #getting week for which daily data is to be aggregated
        self.date = date

        #setting up temp db for aggregation purposes
        self.temp_db.client["Process_Data"]
        self.temp_coll = self.temp_db[tw_handle]

        #retriving required document
        record = self.db[tw_handle].find_one({"date":self.date})

        #inserting record in temp_coll
        self.temp_coll.insert(record)
        return

    def cleanup_tempdb(self):
        self.temp_coll.remove({})
        return

    def get_data_db(self,queryDict):
        
        if "sentiments_line" in queryDict:
            return self.sentiments_line()
        elif "sentiments_pie" in queryDict:
            return self.sentiments_pie()
        elif "tw_metrics" in queryDict:
            if "Reach" in queryDict["tw_metrics"]:
                return self.tw_metrics("Reach")
            elif "Engagement" in queryDict["tw_metrics"]:
                return self.tw_metrics("Engagement")
            elif: "Impression" in queryDict["tw_metrics"]:
                return self.tw_metrics("Impression")

        elif "dayFrequency" in queryDict:
            return self.dayFrequency()

        elif "allFrequency" in queryDict:
            return self.allFrequency()

        elif "tagcloud" in queryDict:
            return self.tagcloud()

    def sentiment_line(self):
        records = self.temp_coll.distinct('Days.sentiments')
##        print records
        sentiDict = dict()

        for itr in range(len(records)):
            tempRecord = records[itr]
            for key in tempRecord.keys():
                if key == '_id':
                    continue
                else:
                    if key not in sentiDict:
                        sentiDict[key] = list()
                    sentiDict[key].append(tempRecord[key])
                    
        return (sentiDict.keys(),sentiDict.values())

    def sentiments_pie(self):
        pipeline = [
            {"$unwind":"$Days"},
            {
                "$group":
                {
                    "_id":"$Days.sentiments._id",
                    "positive":{"$sum":"$Days.sentiments.positive"},
                    "negative":{"$sum":"$Days.sentiments.negative"},
                    "neutral":{"$sum":"$Days.sentiments.neutral"}
                }
            }
        ]

        result = self.temp_coll.aggregate(pipeline)

        sentiAgg = list(result)
        del sentiAgg[0]["_id"]

        labels = sentiAgg[0].keys()
        values = sentiAgg[0].values()
        
        return (labels,values)
        

    def tw_metrics(self,metric):
        records = self.temp_coll.distinct('Days.tw_metrics')
##        print records
        for itr in range(len(records)):
            tempRecord = records[itr]
            for key in tempRecord.keys():
                if key not in metricsDict:
                    metricsDict[key] = list()
                metricsDict[key].append(tempRecord[key])

        metricList = list()
        for key in metricsDict.keys():

            valueList = metricsDict[key].values()
            
            if key == metric:
                for value in valueList:
                    metricList.append(value)
                    
        return (metricList)

    def dayFrequency(self):
        cityList = self.temp_coll.distinct("Days.geo_hist")

        cityDailyDict = dict()
        xlabels = list()

        for i in range(len(cityList)):
            xlabels.append('day_'+str(i+1))
            cityDict = cityList[i]

            for key in cityDict.keys():
                if key not in cityDailyDict:
                    cityDailyDict[key] = list()

                if len(cityDailyDict[key]) != i:
                    for times in range(i - len(cityDailyDict[key])):
                        cityDailyDict[key].append(0)

                cityDailyDict[key].append(cityDict[key])

        print cityDailyDict

        return (cityDailyDict,xlabels)
    

    def allFrequency(self):
        TopCount = -2
        BottomCount = 2

        cityList = self.temp_coll.distinct("Days.geo_hist")
##        print cityList
        cityFreq = dict()

        for i in range(len(cityList)):
            listValue = cityList[i]
            for key in listValue:
                if key != "_id":
                    if key not in cityFreq:
                        cityFreq[key] = 0
                    else:
                        cityFreq[key] += listValue[key]

        #sorting city frequency based on top five and bottom five cities
        cityTopFreq = dict()
        cityBottomFreq = dict()

        cityTopFreq["Others"] = 0
        cityBottomFreq["Others"] = 0
        
        (keys, values) = cityFreq.keys(),cityFreq.values()
        values.sort()
        Top = values[TopCount:]
        Bottom = values[:BottomCount]

        for itr in range(len(Top)):
            for key in cityFreq.keys():
                if cityFreq[key] == Top[itr]:
                    if key not in cityTopFreq:
                        cityTopFreq[key] = Top[itr]

        for itr in range(len(Bottom)):
            for key in cityFreq.keys():
                if cityFreq[key] == Bottom[itr]:
                    if key not in cityBottomFreq:
                        cityBottomFreq[key] = Bottom[itr]

        for key in cityFreq.keys():
            if key not in cityTopFreq:
                cityTopFreq["Others"] += cityFreq[key]

        for key in cityFreq.keys():
            if key not in cityBottomFreq:
                cityBottomFreq["Others"] += cityFreq[key]
                
##        print cityFreq

        return (cityTopFreq,cityBottomFreq)

    def tagcloud(self):
        tagDict = self.temp_coll.distinct('Days.topics')
##        print tagList

        textList = list()
        textString = ""
        
        for i in range(len(tagList)):
            tagDict = tagList[i]
            for j in range(tagDict["count"]):
                textList.append(tagDict["_id"])

        for word in textList:
            textString +=word+", "

        return textString

    
