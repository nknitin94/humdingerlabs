##This module consists of functions that sends aggregated data to the reporting module
##for hour by hour plots of a day specified in "date" parameter

import pymongo
from dbconfig import *

class HourlySummary(object):

    def __init__(self):
        pass

    def get_db_connection(self):
        self.client = pymongo.MongoClient(config_db["host"],config_db["port"])
        self.db = self.client["daily_summary"]
        return

    def get_tw_handles(self,tw_handles):
        self.tw_handles = tw_handles
        return

    def retrieve_day_document(self,date,tw_handle):

        #getting date for which hourly data is to be aggregated
        self.date = date

        #setting up temp db for aggregation purposes
        self.temp_db = self.client["Process_Data"]
        self.temp_coll = self.temp_db[tw_handle]

        #retriving required document
        record = self.db[tw_handle].find_one({"date":self.date})
##        print record

        #inserting record in temp
        self.temp_coll.insert(record)
        return

    def cleanup_temp_db(self):
        #delete temp_db and temp_coll
        self.temp_coll.remove({})
        return
        
    def tagcloud(self):

        topicList = self.temp_coll.distinct('Hours.topics')
##        print topicList
        textList = list()
        textString = ""
        for i in range(len(topicList)):
            textDict = topicList[i]
            for j in range(textDict["count"]):
                if ' ' in textDict["_id"]:
                    textDict["_id"] = textDict["_id"].replace(' ','_')

                textList.append(textDict["_id"])

        for word in textList:
            textString += word+", "

        return textString

    def hrFrequency(self):

        cityList = self.temp_coll.distinct("Hours.geo_hist")
##        print cityList
        
        cityHrDict = dict()
        xlabels = list()

        for i in range(len(cityList)):
            xlabels.append('hr_'+str(i+1))
            cityDict = cityList[i]

            for key in cityDict.keys():
                if key not in cityHrDict:
                    cityHrDict[key] = list()

                if len(cityHrDict[key]) != i:
                    for times in range(i-len(cityHrDict[key])):
                        cityHrDict[key].append(0)

                cityHrDict[key].append(cityDict[key])
##        print cityHrDict
        return (cityHrDict,xlabels)

    def allFrequency(self):

        TopCount = -2
        BottomCount = 2

        cityList = self.temp_coll.distinct("Hours.geo_hist")
##        print cityList
        cityFreq = dict()

        for i in range(len(cityList)):
            listValue = cityList[i]
            for key in listValue:
                if key != "_id":
                    if key not in cityFreq:
                        cityFreq[key] = 0
                    else:
                        cityFreq[key] += listValue[key]

        #sorting city frequency based on top five and bottom five cities
        cityTopFreq = dict()
        cityBottomFreq = dict()

        cityTopFreq["Others"] = 0
        cityBottomFreq["Others"] = 0
        
        (keys, values) = cityFreq.keys(),cityFreq.values()
        values.sort()
        Top = values[TopCount:]
        Bottom = values[:BottomCount]

        for itr in range(len(Top)):
            for key in cityFreq.keys():
                if cityFreq[key] == Top[itr]:
                    if key not in cityTopFreq:
                        cityTopFreq[key] = Top[itr]

        for itr in range(len(Bottom)):
            for key in cityFreq.keys():
                if cityFreq[key] == Bottom[itr]:
                    if key not in cityBottomFreq:
                        cityBottomFreq[key] = Bottom[itr]

        for key in cityFreq.keys():
            if key not in cityTopFreq:
                cityTopFreq["Others"] += cityFreq[key]

        for key in cityFreq.keys():
            if key not in cityBottomFreq:
                cityBottomFreq["Others"] += cityFreq[key]
                
        print cityFreq

        return (cityTopFreq,cityBottomFreq)

    def sentiments_line(self):
        records = self.temp_coll.distinct('Hours.sentiments')
##        print records
        sentiDict = dict()
        for itr in range(len(records)):
            tempRecord = records[itr]
            for key in tempRecord.keys():
                if key == '_id':
                    continue
                else:
                    if key not in sentiDict:
                        sentiDict[key] = list()
                    sentiDict[key].append(tempRecord[key])

##        print sentiDict
        return (sentiDict.keys(),sentiDict.values())

    def sentiments_pie(self):
        pipeline = [
            {"$unwind":"$Hours"},
            {
                "$group":
                {
                    "_id":"$Hours.sentiments._id",
                    "positive":{"$sum":"$Hours.sentiments.positive"},
                    "negative":{"$sum":"$Hours.sentiments.negative"},
                    "neutral":{"$sum":"$Hours.sentiments.neutral"}
                }
            }
        ]

        result = self.temp_coll.aggregate(pipeline)

        sentiAgg = list(result)
        del sentiAgg[0]["_id"]

        labels = sentiAgg[0].keys()
        values = sentiAgg[0].values()
        
        return (labels,values)
    
    def get_data_db(self,queryDict):

        if "sentiments_line" in queryDict.keys():
            return self.sentiments()
        elif "sentiments_pie" in queryDict.keys():
            return self.sentiments_pie()
        elif "tagcloud" in queryDict.keys():
            return self.tagcloud()
        elif "allFrequency" in queryDict.keys():
            return self.allFrequency()
        elif "hrFrequency" in queryDict.keys():
            return self.hrFrequency()

##def main():
##
##    DS = HourlySummary()
##    DS.get_db_connection()
##    DS.get_tw_handles('@chillis')
##    DS.setup_aggregation("Thu Sep 02 2015","@chillis")
##    queryDict = dict()
##    queryDict["tagcloud"] = True
##    DS.tagcloud()
##    DS.get_data_db(queryDict)
##    print Top
##    print Bottom
##    print DS.sentiments()
##
##    DS.cleanup_temp_db()
##    DS.sentiments()
##
##    return
##
##if __name__ == '__main__':
##    main()
