import matplotlib.pyplot as plt
from pylab import *
from pytagcloud import create_tag_image, make_tags
from pytagcloud.lang.counter import get_tag_counts
from db_service import *
import os

class generatePlots(object):

    def __init__(self):
        pass

    def __init__(self,dataBase,collection,summary,customer):

        self.customer = customer
        self.dataBase = dataBase
        self.summary = summary
        self.DBS = DBService(dataBase,collection)

        return

    def get_pie_chart(self):

        #generating query dictionary
        queryDict = dict()
        queryDict["summary"] = self.summary
        queryDict["sentiments"] = self.customer
        
        labels,values = self.DBS.get_data_db(queryDict)

        #creating pie plot
        patches,text,autotexts = plt.pie(values,labels = labels,autopct = '%1.1f%%')
        plt.title('Sentiments for '+self.customer)
        plt.axis('equal')
        plot_filename = self.customer+'_sentiments.png'
        plt.savefig(plot_filename)
        plot_path = os.path.abspath(plot_filename)
        plt.close()

        return plot_path

    def get_tag_cloud(self):

        #generating query dictionary
        queryDict = dict()
        queryDict["summary"] = self.summary
        queryDict["tagcloud"] = self.customer

        #creating tag cloud
        text = self.DBS.get_data_db(queryDict)

        tags = make_tags(get_tag_counts(text))
        plot_filename = self.customer+'_cloud.png'
        create_tag_image(tags,plot_filename,fontname = 'Lobster')
        plot_path = os.path.abspath(plot_filename)

        return plot_path

    def get_bar_chart(self):

        #generating query dictionary
        queryDict = dict()
        queryDict["summary"] = self.summary
        queryDict["allFrequency"] = self.customer

        #creating bar plot
        axis_font = {'fontname':'Arial', 'size':'12'}
        title_font = {'fontname':'Arial', 'size':'16', 'color':'black', 'weight':'normal',
                'verticalalignment':'bottom'}
        
        cityFreq = self.DBS.get_data_db(queryDict)
        
        plot_filename = self.customer+'_city_freq.png'
        plt.bar(range(len(cityFreq)),cityFreq.values(),align = 'center')
        plt.xticks(range(len(cityFreq)),cityFreq.keys())
        plt.xlabel('Locations',axis_font)
        plt.ylabel('Frequency',axis_font)
        plt.title('Distribution of tweets from cites in 24 hours')
        plt.savefig(plot_filename)
        plot_path = os.path.abspath(plot_filename)
        plt.close()
        
        return plot_path

    def get_line_chart(self):

        #generating query dictionary
        queryDict = dict()
        queryDict["summary"] = self.summary
        queryDict["hrFrequency"] = self.customer

        #creating line plot

        (cityHrDict,labels) = self.DBS.get_data_db(queryDict)
                       
        plot_filename = self.customer+'_hr_line.png'
        legendList = list()
        ax = plt.subplot(111)
        x = np.arange(len(labels))
        
        for key in cityHrDict.keys():
            plt.plot(cityHrDict[key])
            legendList.append(key)

        plt.legend(legendList,loc = 'upper right')
        plt.xticks(range(len(labels)),labels,rotation=45)
        plot_path = os.path.abspath(plot_filename)
##        plt.show()
        plt.savefig(plot_filename)
        plt.close()
        return plot_path
        
        


##def main():
##    plots = generatePlots("TwitterTest","HUL_UPDATE","summary_3600","HUL")
##    plots.get_line_chart()
##
##if __name__ == '__main__':
##    main()
        
        

        
        
