from docx import *
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH
import time
from reporting import *
class createReports(object):

    def __init__(self):
        pass
    
    def __init__(self,dataBase,collection,summary,customer):

        self.plots = generatePlots(dataBase,collection,summary,customer)

        #setting up report document
        self.document = Document()
        self.customer = customer
        self.document.add_heading('Twitter Analytics for'+' '+customer, level = 0)
        self.document.add_paragraph(time.strftime("%x")+'\n')

        self.document.add_heading('HumdingerLabs',level = 1)
        self.document.add_page_break()

        return

    def pie_chart(self):

        self.document.add_heading('Sentiments Analysis of Tweets',level = 2)
        self.document.add_picture(self.plots.get_pie_chart(), width = Inches(6.0))
        return

    def bar_chart(self):

        self.document.add_heading('Frequency Distributions of City vs Tweet',level = 2)
        self.document.add_picture(self.plots.get_bar_chart(), width = Inches(6.0))
        return

    def tag_cloud(self):

        self.document.add_picture(self.plots.get_tag_cloud(), width = Inches(6.0))
        return

    def line_chart(self):
        self.document.add_heading('Frequency of tweets per city per hour',level = 2)
        self.document.add_picture(self.plots.get_line_chart(), width = Inches(7.0))
        return
    
    def save_document(self):
        
        self.document.save('report.docx')
        return

def main():
    report = createReports("TwitterTest","HUL_UPDATE","summary_3600","HUL")
    report.pie_chart()
    report.bar_chart()
    report.line_chart()
    report.tag_cloud()
    report.save_document()

    return

if __name__ == '__main__':
    main()
