import pymongo

class DBService(object):
    def __init__(self):
        pass
    
    def __init__(self,dataBase,collection):
        
        self.client = pymongo.MongoClient()
        self.db = self.client[dataBase]
        self.dbCollection = self.db[collection]

    def get_data_db(self,queryDict):

        summary_field = queryDict["summary"]

        if summary_field == "summary_3600": #add other conditions for different summary fields

            if "sentiments" in queryDict:
            
                customer = queryDict["sentiments"]
                query = "$"+summary_field+"."+customer
                pipeline = [
                    {
                    "$unwind":query
                    },
                    {
                        "$group":{
                            "_id": query+".Insights.sentiments._id",
                            "positive":{"$sum":query+".Insights.sentiments.pos"},
                            "negative":{"$sum":query+".Insights.sentiments.neg"},
                            "neutral":{"$sum":query+".Insights.sentiments.neu"}
                        }
                    }
                ]

                result = self.dbCollection.aggregate(pipeline)

                sentiAgg = list(result)
                del sentiAgg[0]["_id"]

                labels = sentiAgg[0].keys()
                values = sentiAgg[0].values()

                return (labels,values)
        
            elif "allFrequency" in queryDict:
            
                customer = queryDict["allFrequency"]
                query = summary_field+"."+customer+".Insights.geo.locations"
                cityList = self.dbCollection.distinct(query)
                cityFreq = dict()

                for i in range(len(cityList)):
                    listValue = cityList[i]
                    for key in listValue:
                        if key != "_id":
                            if key not in cityFreq:
                                cityFreq[key] = 0
                            else:
                                cityFreq[key] += listValue[key]
                

                return cityFreq

            elif "hrFrequency" in queryDict:

                customer = queryDict["hrFrequency"]
                query = summary_field+"."+customer+".Insights.geo.locations"
                cityList = self.dbCollection.distinct(query)
                
                for i in range(len(cityList)):
                    if "_id" in cityList[i]:
                        del cityList[i]["_id"]
                
                cityHrDict = dict()
                xlabels = list()
                
                for i in range(len(cityList)):
                    xlabels.append('hr_'+str(i+1))
                    cityDict = cityList[i]

                    for key in cityDict.keys():
                        if key not in cityHrDict:
                            cityHrDict[key] = list()
                            
                        if len(cityHrDict[key]) != i:
                            for times in range(i - len(cityHrDict[key])):
                                cityHrDict[key].append(0)
                    
                        cityHrDict[key].append(cityDict[key])
                
                return (cityHrDict,xlabels)

            elif "tagcloud" in queryDict:

                customer = queryDict["tagcloud"]
                query = "$"+summary_field+"."+customer+".Insights.topics"

                pipeline = [
                    {
                        "$unwind":"$"+summary_field+"."+customer
                        },
                    {
                        "$unwind":"$"+summary_field+"."+customer+".Insights.topics"
                        },
                    {
                        "$group":{"_id":query,"count":{"$sum":1}}
                        }
                    ]
                
                topicList = list(self.dbCollection.aggregate(pipeline))
                
                textList = list()
                textString = ""
                for i in range(len(topicList)):
                    textDict = topicList[i]
                    for j in range(textDict["count"]):
                        if ' ' in textDict["_id"]:
                            textDict["_id"] = textDict["_id"].replace(' ','_')
                            
                        textList.append(textDict["_id"])
                
                for word in textList:
                    textString += word+", "

                return textString
            
            
            
            
##def main():
##    DBS = DBService("TwitterTest","HUL_UPDATE");
##    queryDict = dict()
##    queryDict["summary"] = "summary_3600"
##    queryDict["hrFrequency"] = "HUL"
##    cityHrDict,labels = DBS.get_data_db(queryDict)
####    print cityHrDict
##    print len(cityHrDict['Kolkata'])
##    print len(labels)
####    queryDict["sentiments"] = "HUL"
####    (labels,values) = DBS.get_data_db(queryDict)
####    print labels
####    print values
####    queryDict["allFrequency"] = "HUL"
####    cityFreq = DBS.get_data_db(queryDict)
####    print cityFreq
##
##if __name__ == '__main__':
##    main()
