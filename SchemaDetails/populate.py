#populates dictStore and stores it in a collection
import pymongo
from random import randint
import store
from store import *

def populate(twitterStore):
  
    for key in range(len(twitterStore["summary_3600"]["HUL"])):
        for sentikey in twitterStore["summary_3600"]["HUL"][key]["Insights"]["sentiments"].keys():
            if sentikey == "_id":
                continue
            else:
                twitterStore["summary_3600"]["HUL"][key]["Insights"]["sentiments"][sentikey] = randint(0,50)
        
        for i in range(5):
            twitterStore["summary_3600"]["HUL"][key]["Insights"]["topics"].append(topicList[randint(0,len(topicList)-1)])
            
        for lockey in twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["locations"].keys():
            if lockey == "_id":
                continue
            else:
                twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["locations"][lockey] = randint(0,50)

        cityValues = twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["locations"].values()
        cityValues = cityValues[1:]
        mst_loc = max(cityValues)
        
        for actkey in twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["locations"].keys():
            if twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["locations"][actkey] == mst_loc:
                twitterStore["summary_3600"]["HUL"][key]["Insights"]["geo"]["mst_act"]["loc"] = actkey
                break
        

def main():
    client = pymongo.MongoClient()
    db = client.TwitterTest
    collection = db.HUL_UPDATE
    populate(dictStore)
    collection.insert(dictStore)

    print dictStore

if __name__ == '__main__':

    main()
